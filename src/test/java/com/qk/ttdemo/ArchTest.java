package com.qk.ttdemo;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.qk.ttdemo");

        noClasses()
            .that()
                .resideInAnyPackage("com.qk.ttdemo.service..")
            .or()
                .resideInAnyPackage("com.qk.ttdemo.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..com.qk.ttdemo.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
