/**
 * View Models used by Spring MVC REST controllers.
 */
package com.qk.ttdemo.web.rest.vm;
